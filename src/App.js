import React, { Component } from 'react';
import './App.css';
import Table from './table';

class App extends React.Component {
  render() {
    return (
     <div>
      <Table />
     </div>
    );
  }
}

export default App;
